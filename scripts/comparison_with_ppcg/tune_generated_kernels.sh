if [ $# -ne 1 ]
then
    echo "No arguments supplied - which architecture? (kepler | tahiti | mali)?"
    exit -1
fi
if [ "$1" == "kepler" ] || [ "$1" == "tahiti" ] || [ "$1" == "mali" ]
then 
echo    # (optional) move to a new line

    FOLDERS=$(find . -name "*Cl" | grep $1)
		N="$(echo $FOLDERS | wc -w)"
		echo -e "\e[31m[WARNING] \e[97mThis script tunes $N kernel-sets each for a max duration of 3h"
		echo -e "\e[31m[WARNING] \e[97mDepending on the machine, the tuning time for one set might also be << 3h"
		echo
		echo -e "\e[31m[WARNING] \e[97mDo you want to continue? (y/n)\e[0m"
		read -p "" -n 1 -r
		if [[ $REPLY =~ ^[Yy]$ ]]
		then
				    for f in $FOLDERS ; do
				            pushd $f > /dev/null
				            KERNELS=$PWD
				            cd $ATF/build
				            do_you_even_tune.sh $KERNELS
				            popd > /dev/null
				    done
		fi
else
    echo "wrong architecture! valid: (kepler | tahiti | mali)?"
    exit -1
fi

