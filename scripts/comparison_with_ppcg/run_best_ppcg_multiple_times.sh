#!/bin/bash

NAME="$(echo $PWD | sed 's/\// /g' | awk '{ print $(NF-3)}')"
echo $NAME

KEY="$(cat cost.csv | head -1 | sed 's/;/ /g' | wc -w)"
BEST="$(cat cost.csv | sort -n -r --field-separator=';' -k $KEY | head -n -1 | tail -1 | sed 's/;/ /g' | sed '$s/\w*$//' | sed -e 's/[[:space:]]*$//')"

# 2d benchmark GS0 GS1 LS0 LS1 TILE0 TILE1
if [ "$(echo $BEST | wc -w)" -eq 6 ]
then
				# remove time
				TILES="$(echo $BEST | awk 'END {print $(NF-1), $NF}' | sed 's/ /,/g')"
				LS="$(echo $BEST | awk 'END {print $(NF-3), $(NF-2)}' | sed 's/ /,/g')"
				GS="$(echo $BEST | awk 'END {print $(NF-5), $(NF-4)}' | sed 's/ /,/g')"
				NDRANGES="$(echo $GS $LS | sed 's/,/ /g')"
				NDRANGES="$(echo $NDRANGES | awk '{$3="1 "$3;print $0}' | awk '{$5=$5" 1";print $0}')"
				GS0="$(echo $GS | sed 's/,/ /g' | awk '{print $1}')"
				GS1="$(echo $GS | sed 's/,/ /g' | awk '{print $2}')"
				LS0="$(echo $LS | sed 's/,/ /g' | awk '{print $1}')"
				LS1="$(echo $LS | sed 's/,/ /g' | awk '{print $2}')"
				GRID0="$(python -c "print ($GS0 / $LS0 )")"
				GRID1="$(python -c "print ($GS1 / $LS1 )")"
				GRID=$GRID0,$GRID1
# 3d benchmark GS0 GS1 GS2 LS0 LS1 LS2 TILE0 TILE1 TILE2
elif [ "$(echo $BEST | wc -w)" -eq 9 ]
then
				TILES="$(echo $BEST | awk 'END {print $(NF-2), $(NF-1), $NF}' | sed 's/ /,/g')"
				LS="$(echo $BEST | awk 'END {print $(NF-5), $(NF-4), $(NF-3)}' | sed 's/ /,/g')"
				GS="$(echo $BEST | awk 'END {print $(NF-8), $(NF-7), $(NF-6)}' | sed 's/ /,/g')"
				NDRANGES="$(echo $GS $LS | sed 's/,/ /g')"
				GS0="$(echo $GS | sed 's/,/ /g' | awk '{print $1}')"
				GS1="$(echo $GS | sed 's/,/ /g' | awk '{print $2}')"
				GS2="$(echo $GS | sed 's/,/ /g' | awk '{print $3}')"
				LS0="$(echo $LS | sed 's/,/ /g' | awk '{print $1}')"
				LS1="$(echo $LS | sed 's/,/ /g' | awk '{print $2}')"
				LS2="$(echo $LS | sed 's/,/ /g' | awk '{print $3}')"
				GRID0="$(python -c "print ($GS0 / $LS0 )")"
				GRID1="$(python -c "print ($GS1 / $LS1 )")"
				GRID2="$(python -c "print ($GS2 / $LS2 )")"
				GRID=$GRID0,$GRID1,$GRID2
else
				echo "something went really wrong"
				exit -1
fi

ppcg --target=opencl --sizes "{ kernel[0] -> tile[$TILES]; kernel[0] -> block[$LS]; kernel[0] -> grid[$GRID] }" *.c 
prepare_ppcg_generated_host.sh
compile_ppcg_ocl.sh *host.c

START=1
for i in $(eval echo "{$START..$ITERATIONS}")
do
	./$NAME $NDRANGES > tmp.txt
	cat tmp.txt
	cat tmp.txt >> out.txt
	cat tmp.txt | head -2 | tail -1 | awk '{print $4}' >> ref.runtime
done

cat ref.runtime
rm tmp.txt
rm *host.c
rm *kernel.cl
