#!/bin/bash
if [ "$#" -ne 4 ]; then
	echo "Illegal number of parameters (enter platform , device id, iterations and inputsize (1024 or 8192)"
	exit -1
fi

N=$3

PWD=$( pwd )    
BASE=$( basename "${PWD}" )
RAWDATA="${BASE}.raw"
OUTDATA="${BASE}.out"

rm $RAWDATA

for i in $(seq 1 $N)
do
./Stencil2D --passes 1 --customSize $4,$4 --lsize 4,128 --weight-center 0.25 --weight-cardinal 0.15 --weight-diagonal 0.05 --num-iters 1 --platform $1 --device $2 >> $RAWDATA 
done

cat $RAWDATA | grep DEBUG | grep -v Performing |  awk '{print $6}' > $OUTDATA
cat $RAWDATA | grep Chose 
cat $RAWDATA | grep CL_DEVICE_MAX_COMPUTE_UNITS
cat $OUTDATA
