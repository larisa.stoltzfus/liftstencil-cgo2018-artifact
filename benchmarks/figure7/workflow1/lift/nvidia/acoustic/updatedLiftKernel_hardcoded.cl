#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple2_float_float;
#endif

float mult(float l, float r){
  { return l * r; }
}
float addTuple(Tuple2_float_float x){
  typedef Tuple2_float_float Tuple;
  
  {return x._0 + x._1;}
}
float idIF(int x){
  { return (float)(x*1.0); }
}
float add(float x, float y){
  { return x+y; }
}
float getCF(int neigh, float cfB, float cfI){
  { if(neigh < 6) { return cfB; } else{ return cfI;} }
}
float multTuple(Tuple2_float_float x){
  typedef Tuple2_float_float Tuple;
  
  {return x._0 * x._1;}
}
float subtract(float l, float r){
  { return l - r; }
}
float id(float x){
  { return x; }
}
int idxF(int i, int j, int k, int m, int n, int o){
  { int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }
}
float subtractTuple(Tuple2_float_float x){
  typedef Tuple2_float_float Tuple;
  
  {return x._0 - x._1;}
}
kernel void KERNEL(const global float* restrict v__41, const global float* restrict v__42, global float* v__85){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__48; 
  float v__51; 
  float v__66; 
  float v__69; 
  float v__70; 
  float v__79; 
  float v__80; 
  /* Private Memory */
  float v__47; 
  float v__50; 
  float v__53; 
  float v__55; 
  float v__57; 
  float v__59; 
  float v__61; 
  float v__63; 
  float v__65; 
  float v__68; 
  float v__72; 
  float v__74; 
  float v__76; 
  float v__78; 
  float v__82; 
  float v__84; 
  for (int v_gl_id_38 = get_global_id(2); v_gl_id_38 < 202; v_gl_id_38 = (v_gl_id_38 + get_global_size(2))) {
    for (int v_gl_id_39 = get_global_id(1); v_gl_id_39 < 256; v_gl_id_39 = (v_gl_id_39 + get_global_size(1))) {
      for (int v_gl_id_40 = get_global_id(0); v_gl_id_40 < 256; v_gl_id_40 = (v_gl_id_40 + get_global_size(0))) {
        v__47 = idIF(idxF(v_gl_id_38, v_gl_id_39, v_gl_id_40, 202, 256, 256)); 
        float v_tmp_185 = 0.3333333f; 
        v__48 = v_tmp_185; 
        v__50 = mult(v__47, v__48); 
        float v_tmp_186 = 2.0f; 
        v__51 = v_tmp_186; 
        v__53 = subtract(v__51, v__50); 
        v__55 = multTuple((Tuple2_float_float){v__53, v__42[(66823 + v_gl_id_40 + (66564 * v_gl_id_38) + (258 * v_gl_id_39))]}); 
        v__57 = add(v__42[(133387 + v_gl_id_40 + (66564 * v_gl_id_38) + (258 * v_gl_id_39))], v__42[(67081 + v_gl_id_40 + (66564 * v_gl_id_38) + (258 * v_gl_id_39))]); 
        v__59 = add(v__57, v__42[(66824 + v_gl_id_40 + (66564 * v_gl_id_38) + (258 * v_gl_id_39))]); 
        v__61 = add(v__59, v__42[(66822 + v_gl_id_40 + (258 * v_gl_id_39) + (66564 * v_gl_id_38))]); 
        v__63 = add(v__61, v__42[(66565 + v_gl_id_40 + (66564 * v_gl_id_38) + (258 * v_gl_id_39))]); 
        v__65 = add(v__63, v__42[(259 + v_gl_id_40 + (258 * v_gl_id_39) + (66564 * v_gl_id_38))]); 
        float v_tmp_187 = 0.3333333f; 
        v__66 = v_tmp_187; 
        v__68 = mult(v__65, v__66); 
        float v_tmp_188 = 0.9971132f; 
        v__69 = v_tmp_188; 
        float v_tmp_189 = 1.0f; 
        v__70 = v_tmp_189; 
        v__72 = getCF(idxF(v_gl_id_38, v_gl_id_39, v_gl_id_40, 202, 256, 256), v__69, v__70); 
        v__74 = mult(v__41[(v_gl_id_40 + (65536 * v_gl_id_38) + (256 * v_gl_id_39))], v__72); 
        v__76 = subtractTuple((Tuple2_float_float){v__68, v__74}); 
        v__78 = addTuple((Tuple2_float_float){v__55, v__76}); 
        float v_tmp_190 = 0.9971216f; 
        v__79 = v_tmp_190; 
        float v_tmp_191 = 1.0f; 
        v__80 = v_tmp_191; 
        v__82 = getCF(idxF(v_gl_id_38, v_gl_id_39, v_gl_id_40, 202, 256, 256), v__79, v__80); 
        v__84 = mult(v__78, v__82); 
        v__85[(v_gl_id_40 + (65536 * v_gl_id_38) + (256 * v_gl_id_39))] = id(v__84); 
      }
    }
  }
}}

