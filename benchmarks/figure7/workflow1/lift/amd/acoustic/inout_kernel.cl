

#define area ((Nx-2)*(Ny-2))
typedef float value;
__kernel void inout(__global value *u,
                    __global value *out,
                    const value ins,
                    const int n)
{	
int localRx = Rx-1;
int localRy = Ry-1;
int localRz = Rz-1;
int  localSx = Sx-1;
int localSy = Sy-1;
int localSz = Sz-1;
	// sum in source
	u[(localSz*area)+(localSy*(Nx-2)+localSx)] += ins;

	// non-interp read out
	out[n]  = u[(localRz*area)+(localRy*(Nx-2)+localRx)];// u[(localRz*area)+(localRy*Nx+localRx)];
	
}
