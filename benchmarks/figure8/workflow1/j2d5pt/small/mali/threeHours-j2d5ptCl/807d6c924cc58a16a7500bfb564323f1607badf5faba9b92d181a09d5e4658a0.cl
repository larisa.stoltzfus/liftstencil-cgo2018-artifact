
// High-level hash: 56c60216968d65daf19c2e65b478557715a8db4825662df35b449853a5bed9af
// Low-level hash: 807d6c924cc58a16a7500bfb564323f1607badf5faba9b92d181a09d5e4658a0

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"


#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__24, global float* v__28){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__27; 
  for (int v_gl_id_18 = get_global_id(1); v_gl_id_18 < 4094; v_gl_id_18 = (v_gl_id_18 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_21 = get_global_id(0); v_gl_id_21 < 4094; v_gl_id_21 = (v_gl_id_21 + GLOBAL_SIZE_0)) {
      v__27 = jacobi(v__24[(1 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(8193 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(4096 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(4098 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(4097 + v_gl_id_21 + (4096 * v_gl_id_18))]); 
      v__28[(4097 + v_gl_id_21 + (4096 * v_gl_id_18))] = id(v__27); 
    }
  }
}}


