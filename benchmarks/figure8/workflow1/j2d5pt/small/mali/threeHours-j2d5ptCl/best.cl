// High-level hash: 56c60216968d65daf19c2e65b478557715a8db4825662df35b449853a5bed9af
// Low-level hash: 807d6c924cc58a16a7500bfb564323f1607badf5faba9b92d181a09d5e4658a0
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__24, global float* v__28){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__27; 
  for (int v_gl_id_18 = get_global_id(1); v_gl_id_18 < 4094; v_gl_id_18 = (v_gl_id_18 + 4096)) {
    for (int v_gl_id_21 = get_global_id(0); v_gl_id_21 < 4094; v_gl_id_21 = (v_gl_id_21 + 32)) {
      v__27 = jacobi(v__24[(1 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(8193 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(4096 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(4098 + v_gl_id_21 + (4096 * v_gl_id_18))], v__24[(4097 + v_gl_id_21 + (4096 * v_gl_id_18))]); 
      v__28[(4097 + v_gl_id_21 + (4096 * v_gl_id_18))] = id(v__27); 
    }
  }
}}
