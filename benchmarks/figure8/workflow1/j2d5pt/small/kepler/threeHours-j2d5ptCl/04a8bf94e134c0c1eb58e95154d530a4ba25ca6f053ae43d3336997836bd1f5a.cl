
// High-level hash: f62cb1a6a4509dd2009c2f06b29816ce49d9f7184b15079f6abe9de74b4855b8
// Low-level hash: 04a8bf94e134c0c1eb58e95154d530a4ba25ca6f053ae43d3336997836bd1f5a

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_53" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_52" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__102, global float* v__105){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__104_0;
  
  for (int v_gl_id_98 = get_global_id(1); v_gl_id_98 < (4094 / v_TP_52); v_gl_id_98 = (v_gl_id_98 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_99 = get_global_id(0); v_gl_id_99 < (4094 / v_TP_53); v_gl_id_99 = (v_gl_id_99 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_100 = 0; v_i_100 < v_TP_52; v_i_100 = (1 + v_i_100)) {
        /* map_seq */
        for (int v_i_101 = 0; v_i_101 < v_TP_53; v_i_101 = (1 + v_i_101)) {
          v__104_0 = jacobi(v__102[(1 + (v_i_101 % v_TP_53) + (4096 * v_TP_52 * v_gl_id_98) + (4096 * (((4094 / v_TP_53) * (v_i_100 % (2 + v_TP_52))) / (4094 / v_TP_53))) + (4096 * (v_gl_id_99 / (4094 / v_TP_53))) + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * (v_i_100 % (2 + v_TP_52)))) % (4094 / v_TP_53))))], v__102[(1 + ((v_i_101 + (2 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / v_TP_53))) + (4096 * (((4094 / v_TP_53) * ((2 + v_i_100) % (2 + v_TP_52))) / (4094 / v_TP_53))) + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * ((2 + v_i_100) % (2 + v_TP_52)))) % (4094 / v_TP_53))))], v__102[(((v_TP_53 + v_i_101) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / v_TP_53))) + (4096 * (((4094 / v_TP_53) * ((1 + v_i_100) % (2 + v_TP_52))) / (4094 / v_TP_53))) + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * ((1 + v_i_100) % (2 + v_TP_52)))) % (4094 / v_TP_53))))], v__102[(2 + ((v_TP_53 + v_i_101) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / v_TP_53))) + (4096 * (((4094 / v_TP_53) * ((1 + v_i_100) % (2 + v_TP_52))) / (4094 / v_TP_53))) + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * ((1 + v_i_100) % (2 + v_TP_52)))) % (4094 / v_TP_53))))], v__102[(1 + ((v_TP_53 + v_i_101) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / v_TP_53))) + (4096 * (((4094 / v_TP_53) * ((1 + v_i_100) % (2 + v_TP_52))) / (4094 / v_TP_53))) + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * ((1 + v_i_100) % (2 + v_TP_52)))) % (4094 / v_TP_53))))]); 
          v__105[(4097 + v_i_101 + (4096 * v_TP_52 * v_gl_id_98) + (-16777216 * (((v_gl_id_99 / (4094 / v_TP_53)) + (((4094 / v_TP_53) * ((v_i_100 + (v_TP_52 * v_gl_id_99)) % v_TP_52)) / (4094 / v_TP_53)) + (v_TP_52 * v_gl_id_98)) / 4095)) + (-4096 * ((v_i_101 + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * ((v_i_100 + (v_TP_52 * v_gl_id_99)) % v_TP_52))) % (4094 / v_TP_53)))) / 4095)) + (4096 * (v_gl_id_99 / (4094 / v_TP_53))) + (4096 * (((4094 / v_TP_53) * ((v_i_100 + (v_TP_52 * v_gl_id_99)) % v_TP_52)) / (4094 / v_TP_53))) + (v_TP_53 * ((v_gl_id_99 + ((4094 / v_TP_53) * ((v_i_100 + (v_TP_52 * v_gl_id_99)) % v_TP_52))) % (4094 / v_TP_53))))] = id(v__104_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


