{ kernel[i0] -> grid[o0, o1] : i0 = 0 and o0 = 256 and o1 = 256; kernel[i0] -> block[o0, o1, o2] : i0 = 0 and o0 = 32 and o1 = 4 and o2 = 4; kernel[i0] -> tile[o0, o1, o2] : i0 = 0 and o0 = 32 and o1 = 32 and o2 = 32 }

size_t global_work_size[3] = {(16) * 32, (16) * 4, 4};
size_t block_size[3] = {32, 4, 4};
