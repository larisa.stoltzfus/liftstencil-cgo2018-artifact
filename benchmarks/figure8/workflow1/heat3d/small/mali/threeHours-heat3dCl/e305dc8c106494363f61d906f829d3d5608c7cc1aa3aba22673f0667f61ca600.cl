
// High-level hash: 660cae58392acd1b84ff4618386905f6b02940bfec7a333667bc0e3cffc65801
// Low-level hash: e305dc8c106494363f61d906f829d3d5608c7cc1aa3aba22673f0667f61ca600

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float heat(float C, float S, float N, float E, float W, float B, float F){
  return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__75, global float* v__82){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__79_0;
  
  for (int v_gl_id_69 = get_global_id(1); v_gl_id_69 < 254; v_gl_id_69 = (v_gl_id_69 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_70 = get_global_id(0); v_gl_id_70 < 254; v_gl_id_70 = (v_gl_id_70 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_71 = 0; v_i_71 < 254; v_i_71 = (1 + v_i_71)) {
        v__79_0 = heat(v__75[(65793 + v_i_71 + (65536 * v_gl_id_69) + (256 * v_gl_id_70))], v__75[(65537 + v_i_71 + (65536 * v_gl_id_69) + (256 * v_gl_id_70))], v__75[(66049 + v_i_71 + (65536 * v_gl_id_69) + (256 * v_gl_id_70))], v__75[(65794 + v_i_71 + (65536 * v_gl_id_69) + (256 * v_gl_id_70))], v__75[(65792 + v_i_71 + (256 * v_gl_id_70) + (65536 * v_gl_id_69))], v__75[(257 + v_i_71 + (256 * v_gl_id_70) + (65536 * v_gl_id_69))], v__75[(131329 + v_i_71 + (65536 * v_gl_id_69) + (256 * v_gl_id_70))]); 
        v__82[(65793 + v_i_71 + (65536 * v_gl_id_69) + (256 * v_gl_id_70))] = id(v__79_0); 
      }
      /* end map_seq */
    }
  }
}}


