// High-level hash: 660cae58392acd1b84ff4618386905f6b02940bfec7a333667bc0e3cffc65801
// Low-level hash: 5968624e9fbb8733b8d8eaec3e412ad19e73877e43749f1368fc208df009e4b8
float heat(float C, float S, float N, float E, float W, float B, float F){
  return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 254; v_gl_id_24 = (v_gl_id_24 + 256)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 254; v_gl_id_25 = (v_gl_id_25 + 256)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 254; v_gl_id_26 = (v_gl_id_26 + 64)) {
        v__29 = heat(v__27[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65537 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(66049 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65794 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65792 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(257 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131329 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))]); 
        v__30[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}
