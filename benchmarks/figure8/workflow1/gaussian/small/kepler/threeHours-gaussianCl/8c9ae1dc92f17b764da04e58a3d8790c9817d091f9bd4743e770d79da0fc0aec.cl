
// High-level hash: b25aafd5344e2613607aeacf0ac8b39af4010fc888fe7aaa7d2630b91f7a6de9
// Low-level hash: 8c9ae1dc92f17b764da04e58a3d8790c9817d091f9bd4743e770d79da0fc0aec

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__62, global float* v__65){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__64_0;
  
  for (int v_wg_id_57 = get_group_id(1); v_wg_id_57 < (4092 / v_TP_41); v_wg_id_57 = (v_wg_id_57 + NUM_GROUPS_1)) {
    for (int v_wg_id_59 = get_group_id(0); v_wg_id_59 < (4092 / v_TP_42); v_wg_id_59 = (v_wg_id_59 + NUM_GROUPS_0)) {
      for (int v_l_id_60 = get_local_id(1); v_l_id_60 < v_TP_41; v_l_id_60 = (v_l_id_60 + LOCAL_SIZE_1)) {
        for (int v_l_id_61 = get_local_id(0); v_l_id_61 < v_TP_42; v_l_id_61 = (v_l_id_61 + LOCAL_SIZE_0)) {
          v__64_0 = jacobi(v__62[((v_l_id_61 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (4096 * (((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41))) / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(1 + (v_l_id_61 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (4096 * (((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41))) / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(2 + (v_l_id_61 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (4096 * (((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41))) / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(3 + (v_l_id_61 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (4096 * (((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41))) / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(4 + (v_l_id_61 % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (4096 * (((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41))) / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * (v_l_id_60 % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(1 + ((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(2 + ((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(3 + ((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(4 + ((v_TP_42 + v_l_id_61) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((1 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(((v_l_id_61 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(1 + ((v_l_id_61 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(2 + ((v_l_id_61 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(3 + ((v_l_id_61 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(4 + ((v_l_id_61 + (2 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((2 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(((v_l_id_61 + (3 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(1 + ((v_l_id_61 + (3 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(2 + ((v_l_id_61 + (3 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(3 + ((v_l_id_61 + (3 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(4 + ((v_l_id_61 + (3 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((3 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(((v_l_id_61 + (4 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(1 + ((v_l_id_61 + (4 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(2 + ((v_l_id_61 + (4 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(3 + ((v_l_id_61 + (4 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))], v__62[(4 + ((v_l_id_61 + (4 * v_TP_42)) % v_TP_42) + (4096 * v_TP_41 * v_wg_id_57) + (4096 * (((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41))) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((4 + v_l_id_60) % (4 + v_TP_41)))) % (4092 / v_TP_42))))]); 
          v__65[(8194 + v_l_id_61 + (-4096 * ((v_l_id_61 + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41))) % (4092 / v_TP_42)))) / 4095)) + (4096 * v_TP_41 * v_wg_id_57) + (-16777216 * (((((4092 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41)) / (4092 / v_TP_42)) + (v_wg_id_59 / (4092 / v_TP_42)) + (v_TP_41 * v_wg_id_57)) / 4095)) + (4096 * (((4092 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41)) / (4092 / v_TP_42))) + (4096 * (v_wg_id_59 / (4092 / v_TP_42))) + (v_TP_42 * ((v_wg_id_59 + ((4092 / v_TP_42) * ((v_l_id_60 + (v_TP_41 * v_wg_id_59)) % v_TP_41))) % (4092 / v_TP_42))))] = id(v__64_0); 
        }
      }
    }
  }
}}


