// High-level hash: cf9fea2a31fa72c45f3d3100da7343e7a4f3b05761ff36852c5bc3fb51a49c8f
// Low-level hash: cbd04d6fa56212df52fccc7fcc58119c408712e857f2b5bf5acd98fec6b850b9
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__22, global float* v__27){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__26; 
  for (int v_gl_id_19 = get_global_id(0); v_gl_id_19 < 4094; v_gl_id_19 = (v_gl_id_19 + 4096)) {
    for (int v_gl_id_21 = get_global_id(1); v_gl_id_21 < 4094; v_gl_id_21 = (v_gl_id_21 + 256)) {
      v__26 = grad(v__22[(1 + v_gl_id_21 + (4096 * v_gl_id_19))], v__22[(8193 + v_gl_id_21 + (4096 * v_gl_id_19))], v__22[(4096 + v_gl_id_21 + (4096 * v_gl_id_19))], v__22[(4098 + v_gl_id_21 + (4096 * v_gl_id_19))], v__22[(4097 + v_gl_id_21 + (4096 * v_gl_id_19))]); 
      v__27[(4097 + v_gl_id_21 + (4096 * v_gl_id_19))] = id(v__26); 
    }
  }
}}
