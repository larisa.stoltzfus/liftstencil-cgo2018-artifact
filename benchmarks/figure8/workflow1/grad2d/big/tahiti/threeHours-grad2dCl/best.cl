// High-level hash: d107accb66989f13d5ff8312c84e7ab00497b1e5dbf66865194e802d8efd31fc
// Low-level hash: 8a64e1a76d2c9193165c47608d2997de73bb1fc01e1170322079dec986818019
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 8190; v_gl_id_12 = (v_gl_id_12 + 8192)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 8190; v_gl_id_13 = (v_gl_id_13 + 4096)) {
      v__16 = grad(v__14[(1 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16385 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8192 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8194 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))]); 
      v__17[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))] = id(v__16); 
    }
  }
}}
