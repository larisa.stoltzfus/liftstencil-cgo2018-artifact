
// High-level hash: 00a98ff96921e14b0a740f74ed8a1e924740c8254c099f4a4cdfe88ed7c9badb
// Low-level hash: 857b059d68d09f263cf3e54d8a87624c1ac04e09c1a9968694cd77cab2487126

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_542" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_541" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__561, global float* v__565){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__562[(4+(2*v_TP_541)+(2*v_TP_542)+(v_TP_541*v_TP_542))];
  /* Typed Value memory */
  /* Private Memory */
  float v__564_0;
  
  for (int v_wg_id_555 = get_group_id(1); v_wg_id_555 < (8190 / v_TP_541); v_wg_id_555 = (v_wg_id_555 + NUM_GROUPS_1)) {
    for (int v_wg_id_556 = get_group_id(0); v_wg_id_556 < (8190 / v_TP_542); v_wg_id_556 = (v_wg_id_556 + NUM_GROUPS_0)) {
      for (int v_l_id_557 = get_local_id(1); v_l_id_557 < (2 + v_TP_541); v_l_id_557 = (v_l_id_557 + LOCAL_SIZE_1)) {
        for (int v_l_id_558 = get_local_id(0); v_l_id_558 < (2 + v_TP_542); v_l_id_558 = (v_l_id_558 + LOCAL_SIZE_0)) {
          v__562[(v_l_id_558 + (2 * v_l_id_557) + (v_TP_542 * v_l_id_557))] = idfloat(v__561[(v_l_id_558 + (8192 * v_TP_541 * v_wg_id_555) + (8192 * (v_wg_id_556 / (8190 / v_TP_542))) + (8192 * (((8190 / v_TP_542) * (v_l_id_557 % (2 + v_TP_541))) / (8190 / v_TP_542))) + (v_TP_542 * ((v_wg_id_556 + ((8190 / v_TP_542) * (v_l_id_557 % (2 + v_TP_541)))) % (8190 / v_TP_542))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_559 = get_local_id(1); v_l_id_559 < v_TP_541; v_l_id_559 = (v_l_id_559 + LOCAL_SIZE_1)) {
        for (int v_l_id_560 = get_local_id(0); v_l_id_560 < v_TP_542; v_l_id_560 = (v_l_id_560 + LOCAL_SIZE_0)) {
          v__564_0 = jacobi(v__562[((v_l_id_560 % v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(1 + (v_l_id_560 % v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(2 + (v_l_id_560 % v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(2 + v_TP_542 + ((v_TP_542 + v_l_id_560) % v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(3 + v_TP_542 + ((v_TP_542 + v_l_id_560) % v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(4 + v_TP_542 + ((v_TP_542 + v_l_id_560) % v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(4 + v_l_id_560 + (2 * v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(5 + v_l_id_560 + (2 * v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))], v__562[(6 + v_l_id_560 + (2 * v_TP_542) + (2 * v_l_id_559) + (v_TP_542 * v_l_id_559))]); 
          v__565[(8193 + v_l_id_560 + (-8192 * ((v_l_id_560 + (v_TP_542 * ((v_wg_id_556 + ((8190 / v_TP_542) * ((v_l_id_559 + (v_TP_541 * v_wg_id_556)) % v_TP_541))) % (8190 / v_TP_542)))) / 8191)) + (8192 * v_TP_541 * v_wg_id_555) + (-67108864 * (((((8190 / v_TP_542) * ((v_l_id_559 + (v_TP_541 * v_wg_id_556)) % v_TP_541)) / (8190 / v_TP_542)) + (v_wg_id_556 / (8190 / v_TP_542)) + (v_TP_541 * v_wg_id_555)) / 8191)) + (8192 * (((8190 / v_TP_542) * ((v_l_id_559 + (v_TP_541 * v_wg_id_556)) % v_TP_541)) / (8190 / v_TP_542))) + (8192 * (v_wg_id_556 / (8190 / v_TP_542))) + (v_TP_542 * ((v_wg_id_556 + ((8190 / v_TP_542) * ((v_l_id_559 + (v_TP_541 * v_wg_id_556)) % v_TP_541))) % (8190 / v_TP_542))))] = id(v__564_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


