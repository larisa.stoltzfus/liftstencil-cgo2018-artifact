
// High-level hash: c8811d8271478b4ed36deb17467fc194c184f2f00cdfce1fcf12efcacb29b052
// Low-level hash: 857b059d68d09f263cf3e54d8a87624c1ac04e09c1a9968694cd77cab2487126

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_98" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_97" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__149, global float* v__200){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__182[(4+(2*v_TP_97)+(2*v_TP_98)+(v_TP_97*v_TP_98))];
  /* Typed Value memory */
  /* Private Memory */
  float v__199_0;
  
  for (int v_wg_id_142 = get_group_id(1); v_wg_id_142 < (8190 / v_TP_97); v_wg_id_142 = (v_wg_id_142 + NUM_GROUPS_1)) {
    for (int v_wg_id_144 = get_group_id(0); v_wg_id_144 < (8190 / v_TP_98); v_wg_id_144 = (v_wg_id_144 + NUM_GROUPS_0)) {
      for (int v_l_id_145 = get_local_id(1); v_l_id_145 < (2 + v_TP_97); v_l_id_145 = (v_l_id_145 + LOCAL_SIZE_1)) {
        for (int v_l_id_146 = get_local_id(0); v_l_id_146 < (2 + v_TP_98); v_l_id_146 = (v_l_id_146 + LOCAL_SIZE_0)) {
          v__182[(v_l_id_146 + (2 * v_l_id_145) + (v_TP_98 * v_l_id_145))] = idfloat(v__149[(v_l_id_146 + (8192 * v_TP_97 * v_wg_id_142) + (8192 * (v_wg_id_144 / (8190 / v_TP_98))) + (8192 * (((8190 / v_TP_98) * (v_l_id_145 % (2 + v_TP_97))) / (8190 / v_TP_98))) + (v_TP_98 * ((v_wg_id_144 + ((8190 / v_TP_98) * (v_l_id_145 % (2 + v_TP_97)))) % (8190 / v_TP_98))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_147 = get_local_id(1); v_l_id_147 < v_TP_97; v_l_id_147 = (v_l_id_147 + LOCAL_SIZE_1)) {
        for (int v_l_id_148 = get_local_id(0); v_l_id_148 < v_TP_98; v_l_id_148 = (v_l_id_148 + LOCAL_SIZE_0)) {
          v__199_0 = jacobi(v__182[((v_l_id_148 % v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(1 + (v_l_id_148 % v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(2 + (v_l_id_148 % v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(2 + v_TP_98 + ((v_TP_98 + v_l_id_148) % v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(3 + v_TP_98 + ((v_TP_98 + v_l_id_148) % v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(4 + v_TP_98 + ((v_TP_98 + v_l_id_148) % v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(4 + v_l_id_148 + (2 * v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(5 + v_l_id_148 + (2 * v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))], v__182[(6 + v_l_id_148 + (2 * v_TP_98) + (2 * v_l_id_147) + (v_TP_98 * v_l_id_147))]); 
          v__200[(8193 + v_l_id_148 + (-8192 * ((v_l_id_148 + (v_TP_98 * ((v_wg_id_144 + ((8190 / v_TP_98) * ((v_l_id_147 + (v_TP_97 * v_wg_id_144)) % v_TP_97))) % (8190 / v_TP_98)))) / 8191)) + (8192 * v_TP_97 * v_wg_id_142) + (-67108864 * (((((8190 / v_TP_98) * ((v_l_id_147 + (v_TP_97 * v_wg_id_144)) % v_TP_97)) / (8190 / v_TP_98)) + (v_wg_id_144 / (8190 / v_TP_98)) + (v_TP_97 * v_wg_id_142)) / 8191)) + (8192 * (((8190 / v_TP_98) * ((v_l_id_147 + (v_TP_97 * v_wg_id_144)) % v_TP_97)) / (8190 / v_TP_98))) + (8192 * (v_wg_id_144 / (8190 / v_TP_98))) + (v_TP_98 * ((v_wg_id_144 + ((8190 / v_TP_98) * ((v_l_id_147 + (v_TP_97 * v_wg_id_144)) % v_TP_97))) % (8190 / v_TP_98))))] = id(v__199_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


