
// High-level hash: 240eaceda3c42cdd13f7e09bcaa2ed2c945d4005470ea7188d3cf3c3e8d75d40
// Low-level hash: ec33646cd4633fa1ff9b7e12a203bd054e2fb87b7b1303908afe3d8f2874f742

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_1201" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_1200" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__1220, global float* v__1224){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__1221[(4+(2*v_TP_1200)+(2*v_TP_1201)+(v_TP_1200*v_TP_1201))];
  /* Typed Value memory */
  /* Private Memory */
  float v__1223_0;
  
  for (int v_wg_id_1214 = get_group_id(0); v_wg_id_1214 < (4094 / v_TP_1200); v_wg_id_1214 = (v_wg_id_1214 + NUM_GROUPS_0)) {
    for (int v_wg_id_1215 = get_group_id(1); v_wg_id_1215 < (4094 / v_TP_1201); v_wg_id_1215 = (v_wg_id_1215 + NUM_GROUPS_1)) {
      for (int v_l_id_1216 = get_local_id(0); v_l_id_1216 < (2 + v_TP_1200); v_l_id_1216 = (v_l_id_1216 + LOCAL_SIZE_0)) {
        for (int v_l_id_1217 = get_local_id(1); v_l_id_1217 < (2 + v_TP_1201); v_l_id_1217 = (v_l_id_1217 + LOCAL_SIZE_1)) {
          v__1221[(v_l_id_1217 + (2 * v_l_id_1216) + (v_TP_1201 * v_l_id_1216))] = idfloat(v__1220[(v_l_id_1217 + (4096 * v_TP_1200 * v_wg_id_1214) + (4096 * (v_wg_id_1215 / (4094 / v_TP_1201))) + (4096 * (((4094 / v_TP_1201) * (v_l_id_1216 % (2 + v_TP_1200))) / (4094 / v_TP_1201))) + (v_TP_1201 * ((v_wg_id_1215 + ((4094 / v_TP_1201) * (v_l_id_1216 % (2 + v_TP_1200)))) % (4094 / v_TP_1201))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_1218 = get_local_id(0); v_l_id_1218 < v_TP_1200; v_l_id_1218 = (v_l_id_1218 + LOCAL_SIZE_0)) {
        for (int v_l_id_1219 = get_local_id(1); v_l_id_1219 < v_TP_1201; v_l_id_1219 = (v_l_id_1219 + LOCAL_SIZE_1)) {
          v__1223_0 = jacobi(v__1221[((v_l_id_1219 % v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(1 + (v_l_id_1219 % v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(2 + (v_l_id_1219 % v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(2 + v_TP_1201 + ((v_TP_1201 + v_l_id_1219) % v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(3 + v_TP_1201 + ((v_TP_1201 + v_l_id_1219) % v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(4 + v_TP_1201 + ((v_TP_1201 + v_l_id_1219) % v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(4 + v_l_id_1219 + (2 * v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(5 + v_l_id_1219 + (2 * v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))], v__1221[(6 + v_l_id_1219 + (2 * v_TP_1201) + (2 * v_l_id_1218) + (v_TP_1201 * v_l_id_1218))]); 
          v__1224[(4097 + v_l_id_1219 + (-4096 * ((v_l_id_1219 + (v_TP_1201 * ((v_wg_id_1215 + ((4094 / v_TP_1201) * ((v_l_id_1218 + (v_TP_1200 * v_wg_id_1215)) % v_TP_1200))) % (4094 / v_TP_1201)))) / 4095)) + (4096 * v_TP_1200 * v_wg_id_1214) + (-16777216 * (((((4094 / v_TP_1201) * ((v_l_id_1218 + (v_TP_1200 * v_wg_id_1215)) % v_TP_1200)) / (4094 / v_TP_1201)) + (v_wg_id_1215 / (4094 / v_TP_1201)) + (v_TP_1200 * v_wg_id_1214)) / 4095)) + (4096 * (((4094 / v_TP_1201) * ((v_l_id_1218 + (v_TP_1200 * v_wg_id_1215)) % v_TP_1200)) / (4094 / v_TP_1201))) + (4096 * (v_wg_id_1215 / (4094 / v_TP_1201))) + (v_TP_1201 * ((v_wg_id_1215 + ((4094 / v_TP_1201) * ((v_l_id_1218 + (v_TP_1200 * v_wg_id_1215)) % v_TP_1200))) % (4094 / v_TP_1201))))] = id(v__1223_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


