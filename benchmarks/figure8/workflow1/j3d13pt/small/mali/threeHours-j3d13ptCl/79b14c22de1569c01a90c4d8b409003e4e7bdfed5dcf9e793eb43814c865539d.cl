
// High-level hash: 952ed6e585e8c66c2ee49bae315e8147db8650df17b5cb3b39f687a1192470de
// Low-level hash: 79b14c22de1569c01a90c4d8b409003e4e7bdfed5dcf9e793eb43814c865539d

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float jacobi(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
  return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__84, global float* v__87){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__86; 
  for (int v_gl_id_81 = get_global_id(0); v_gl_id_81 < 252; v_gl_id_81 = (v_gl_id_81 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_82 = get_global_id(1); v_gl_id_82 < 252; v_gl_id_82 = (v_gl_id_82 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_83 = get_global_id(2); v_gl_id_83 < 252; v_gl_id_83 = (v_gl_id_83 + GLOBAL_SIZE_2)) {
        v__86 = jacobi(v__84[(131588 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(131587 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(131585 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(131584 + v_gl_id_83 + (256 * v_gl_id_82) + (65536 * v_gl_id_81))], v__84[(132098 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(131842 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(131330 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(131074 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(262658 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(197122 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(66050 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))], v__84[(514 + v_gl_id_83 + (256 * v_gl_id_82) + (65536 * v_gl_id_81))], v__84[(131586 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))]); 
        v__87[(131586 + v_gl_id_83 + (65536 * v_gl_id_81) + (256 * v_gl_id_82))] = id(v__86); 
      }
    }
  }
}}


