-- test.hs

import System.IO  
import Control.Monad

value1 :: Int
value1 = <$TP:N>

value2 :: Int
value2 = <$TP:M>


main = do
        file <- openFile "costfile.txt" WriteMode
        hPrint file (value1 + value2)
        hClose file





