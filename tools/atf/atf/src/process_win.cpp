// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#include <detail/platform.hpp>

#ifdef LIBATF_IS_WINDOWS

#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <windows.h>

#include <detail/process.hpp>

namespace atf {
namespace detail {
auto ascii_to_wide(const ::std::string &p_str) -> ::std::wstring {
  // Determine destination length
  const auto t_len =
      MultiByteToWideChar(CP_ACP, 0, p_str.c_str(), p_str.length() + 1, 0, 0);

  // Create wstring and adjust size
  ::std::wstring t_dest{};
  t_dest.resize(t_len);

  // Convert
  MultiByteToWideChar(CP_ACP, 0, p_str.c_str(), p_str.length() + 1, &t_dest[0],
                      t_len);

  return t_dest;
}

process::process(const ::std::string &p_path, const ::std::string &p_args)
    : process(defer_exec, p_path, p_args) {
  run();
}

process::process(defer_exec_t, const ::std::string &p_path,
                 const ::std::string &p_args)
    : m_Path{p_path}, m_Args{p_args} {}

process::~process() {
  if (running())
    ::std::terminate();
}

auto process::running() const -> bool { return m_Handle != handle_type{}; }

auto process::detach() -> void { m_Handle = handle_type{}; }

auto process::handle() const -> handle_type { return m_Handle; }

auto process::run() -> void {
  if (running())
    throw ::std::runtime_error("process::run: Already running!");

  // Setup data structures
  STARTUPINFO t_si;
  PROCESS_INFORMATION t_pi;

  ZeroMemory(&t_si, sizeof(t_si));
  t_si.cb = sizeof(t_si);
  ZeroMemory(&t_pi, sizeof(t_pi));

  // TODO the first element of the command line needs to be the application name

#ifdef UNICODE
  // Convert path and commandline to wide strings
  const auto t_path = ascii_to_wide(m_Path);
  const auto t_args = ascii_to_wide(m_Args);
#endif

  // Try to launch process
  const auto t_result = CreateProcess(
#ifdef UNICODE
      t_path.c_str(), &t_args[0],
#else
      m_Path.c_str(), &m_Args[0],
#endif
      NULL, NULL, FALSE, 0, NULL, NULL, &t_si, &t_pi);

  if (!t_result)
    throw ::std::runtime_error("process::run: Failed to CreateProcess()");
  else {
    // Success. Save process handle
    m_Handle = reinterpret_cast<handle_type>(t_pi.hProcess);
  }
}

auto process::wait() -> bool {
  if (!running())
    throw ::std::runtime_error("process::wait: Called while not running!");

  // Convert handle to native type
  const auto t_handle = reinterpret_cast<HANDLE>(m_Handle);

  if (WaitForSingleObject(t_handle, INFINITE) == WAIT_FAILED)
    throw ::std::runtime_error(
        "process::wait: Failed to WaitForSingleObject()!");

  // Clear handle
  m_Handle = handle_type{};

  DWORD t_returncode;
  if (GetExitCodeProcess(t_handle, &t_returncode) == FALSE)
    throw ::std::runtime_error(
        "process::wait: Failed to GetExitCodeProcess()!");
  else
    return t_returncode == EXIT_SUCCESS;
}
} // namespace detail
} // namespace atf

#endif
