//
//  main.cpp
//
//
//  Created by Ari Rasch on 28/10/16.
//
//

#include <array>
#include <math.h>
#include <random>
#include <stddef.h>
#include <string>
#include <type_traits>
#include <utility>

#include "atf.h"

#define MD_HOM_GEMM

//#define CLBLAST_SAXPY
//#define CLBLAST_GEMM_DIRECT

//#define CLTUNE_CONV
//#define CLTUNE_MATMULT

//#define SEARCH_SPACE_GENERATION_TEST_USING_GEMM

//#define MD_HOM_GEMV

#ifdef MD_HOM_GEMM
void fill(std::vector<float> &mat) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<float> dist(1.0, 10.0);

  for (size_t i = 0; i < mat.size(); ++i)
    mat[i] = dist(mt);
}

void zero(std::vector<float> &mat) {
  for (size_t i = 0; i < mat.size(); ++i)
    mat[i] = 0; // dist(mt);
}

int main() {
  auto start = std::chrono::system_clock::now();

  std::string gemm_ocl_source =
#include "md_hom_gemm.cl"
      ;

  const size_t N = 2;
  const int N_3 = pow(2, N);
  const int N_2 = pow(2, N);
  const int N_1 = pow(2, N);

  std::vector<float> A(N_3 * N_1);
  fill(A);
  std::vector<float> B(N_1 * N_2);
  fill(B);
  std::vector<float> C(N_3 * N_2);
  zero(C);

  std::vector<float> C_gold(N_3 * N_2);
  for (int i = 0; i < N_3; ++i)
    for (int j = 0; j < N_2; ++j)
      for (int k = 0; k < N_1; ++k)
        C_gold[i * N_2 + j] += A[i * N_1 + k] * B[k * N_2 + j];

#if 0 // only for testing
  for( int i = 0 ; i < N_3 ; ++i )
    for( int j = 0 ; j < N_2 ; ++j )
      std::cout << "C_gold[ i * kSizeN + j ] = " << C_gold[ i * N_2 + j ] << std::endl;
#endif

// TPs
#if 1
  auto NUM_WG_3 = atf::tp(
      "NUM_WG_3",
      atf::interval<size_t>(0, N, atf::pow_2)); // , atf::divides( N_3 ) );
  auto NUM_WG_2 = atf::tp(
      "NUM_WG_2",
      atf::interval<size_t>(0, N, atf::pow_2)); //, atf::divides( N_2 ) );
  auto NUM_WG_1 =
      atf::tp("NUM_WG_1", {1}); // atf::interval<size_t>(0,N, atf::pow_2) );

  auto NUM_WI_3 = atf::tp(
      "NUM_WI_3", atf::interval<size_t>(
                      0, N, atf::pow_2)); //, atf::divides( N_3 / NUM_WG_3 ) );
  auto NUM_WI_2 = atf::tp(
      "NUM_WI_2", atf::interval<size_t>(
                      0, N, atf::pow_2)); //, atf::divides( N_2 / NUM_WG_2 ) );
  auto NUM_WI_1 = atf::tp(
      "NUM_WI_1",
      atf::interval<size_t>(0, N, atf::pow_2)); //, atf::divides( N_1 / NUM_WG_1
                                                //) ); // has to be a power of 2
                                                //(currently required for
                                                //parallel reduction)

  auto LM_SIZE_3 = atf::tp(
      "LM_SIZE_3", atf::interval<size_t>(0, N, atf::pow_2),
      [&](auto LM_SIZE_3) { return N_3 % (NUM_WG_3 * LM_SIZE_3) == 0; });
  auto LM_SIZE_2 = atf::tp(
      "LM_SIZE_2", atf::interval<size_t>(0, N, atf::pow_2),
      [&](auto LM_SIZE_2) { return N_2 % (NUM_WG_2 * LM_SIZE_2) == 0; });
  auto LM_SIZE_1 = atf::tp(
      "LM_SIZE_1", atf::interval<size_t>(0, N, atf::pow_2),
      [&](auto LM_SIZE_1) { return N_1 % (NUM_WG_1 * LM_SIZE_1) == 0; });

  auto PM_SIZE_3 = atf::tp(
      "PM_SIZE_3", atf::interval<size_t>(0, N, atf::pow_2),
      [&](auto PM_SIZE_3) { return LM_SIZE_3 % (NUM_WI_3 * PM_SIZE_3) == 0; });
  auto PM_SIZE_2 = atf::tp(
      "PM_SIZE_2", atf::interval<size_t>(0, N, atf::pow_2),
      [&](auto PM_SIZE_2) { return LM_SIZE_2 % (NUM_WI_2 * PM_SIZE_2) == 0; });
  auto PM_SIZE_1 = atf::tp(
      "PM_SIZE_1", atf::interval<size_t>(0, N, atf::pow_2),
      [&](auto PM_SIZE_1) { return LM_SIZE_1 % (NUM_WI_1 * PM_SIZE_1) == 0; });

  auto GM_SIZE_3 = atf::tp("GM_SIZE_3", {N_3});
  auto GM_SIZE_2 = atf::tp("GM_SIZE_2", {N_2});
  auto GM_SIZE_1 = atf::tp("GM_SIZE_1", {N_1});
#endif

// only for testing
#if 0
  auto NUM_WG_3 = atf::tp( "NUM_WG_3", {1} );
  auto NUM_WG_2 = atf::tp( "NUM_WG_2", {1} );
  auto NUM_WG_1 = atf::tp( "NUM_WG_1", {1} );

  auto NUM_WI_3 = atf::tp( "NUM_WI_3", {2} );
  auto NUM_WI_2 = atf::tp( "NUM_WI_2", {1} );
  auto NUM_WI_1 = atf::tp( "NUM_WI_1", {1} );

  auto LM_SIZE_3 = atf::tp( "LM_SIZE_3", {4} );
  auto LM_SIZE_2 = atf::tp( "LM_SIZE_2", {4} );
  auto LM_SIZE_1 = atf::tp( "LM_SIZE_1", {4} );

  auto PM_SIZE_3 = atf::tp( "PM_SIZE_3", {2} );
  auto PM_SIZE_2 = atf::tp( "PM_SIZE_2", {4} );
  auto PM_SIZE_1 = atf::tp( "PM_SIZE_1", {4} );
  
  auto GM_SIZE_3 = atf::tp( "GM_SIZE_3", { N_3 } );
  auto GM_SIZE_2 = atf::tp( "GM_SIZE_2", { N_2 } );
  auto GM_SIZE_1 = atf::tp( "GM_SIZE_1", { N_1 } );
#endif

  // OpenCL kernel wrapper
  auto ocl_kernel =
      atf::cf::ocl({"Apple", atf::cf::device_info::GPU, 0},
                   {gemm_ocl_source, "gemm_1"},
                   inputs(atf::buffer<float>(A), atf::buffer<float>(B),
                          atf::buffer<float>(C)),
                   atf::cf::GS(NUM_WG_1 * NUM_WI_1, NUM_WG_2 * NUM_WI_2,
                               NUM_WG_3 * NUM_WI_3),
                   atf::cf::LS(NUM_WI_1, NUM_WI_2, NUM_WI_3))
          .check_result(A, B, C_gold);

  auto best_config = atf::exhaustive //<NO_CONSTRAINTS>()
      // atf::open_tuner//<NO_CONSTRAINTS>
      // atf::open_tuner_flat<NO_CONSTRAINTS>
      // atf::annealing_tree<NO_CONSTRAINTS>
      // atf::annealing<NO_CONSTRAINTS>
      ()
      //( atf::cond::evaluations(1) )
      //( atf::cond::speedup(1, 200) || atf::cond::evaluations(1000) )
      //( atf::cond::speedup(1, 200) )

      (G(NUM_WG_3, NUM_WI_3, LM_SIZE_3, PM_SIZE_3, GM_SIZE_3),
       G(NUM_WG_2, NUM_WI_2, LM_SIZE_2, PM_SIZE_2, GM_SIZE_2),
       G(NUM_WG_1, NUM_WI_1, LM_SIZE_1, PM_SIZE_1, GM_SIZE_1))(ocl_kernel);

  std::cout << "\nbest found configuration: ";
  for (auto &tp : best_config)
    std::cout << tp.first << " = " << tp.second << std::endl;

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec =
      std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
  std::cout << std::endl
            << "total runtime for tuning and search space generation = "
            << runtime_in_sec << "sec\n"
            << std::endl;

  return 0;
}
#endif

#ifdef CLBLAST_SAXPY

int main() {
  auto start = std::chrono::system_clock::now();

  std::string saxpy_ocl_source =
#include "saxpy.cl" // TODO: USE_CL_MAD set?
      ;

  // input size
  const int n = 1024;

  // alpha
  const float alpha = 1;

  // x (lhs)
  auto x = std::vector<float>(n, 1);
  const int x_offset = 0;
  const int x_inc = 1;

  // y (rhs)
  auto y = std::vector<float>(n, 2);
  const int y_offset = 0;
  const int y_inc = 1;

  // tuning parameters
  auto WGS = atf::tp("WGS", {64, 128, 256, 512, 1024, 2048});
  auto WPT = atf::tp("WPT", {1, 2, 4, 8});
  auto VW = atf::tp("VW", {1, 2, 4, 8});

  // global/local-sizes
  auto GS_0 = atf::tp("GS_0", atf::interval<size_t>(1, 1, [&](auto) -> size_t {
                        return (n / WPT) / VW;
                      }));
  auto LS_0 = atf::tp(
      "LS_0", atf::interval<size_t>(1, 1, [&](auto) -> size_t { return WGS; }));

  // cost function (TODO: CLBlast wrapper)
  auto ocl_kernel =
      atf::cf::ocl("Apple", CL_DEVICE_TYPE_CPU, 0, saxpy_ocl_source,
                   inputs(atf::scalar<int>(n),

                          atf::scalar<float>(alpha),

                          atf::buffer<float>(n), atf::scalar<int>(x_offset),
                          atf::scalar<int>(x_inc),

                          atf::buffer<float>(n), atf::scalar<int>(y_offset),
                          atf::scalar<int>(y_inc)));

  // tuning
  atf::open_tuner_flat(atf::cond::valid_test_count(10))
      .set_TPs(WGS, WPT, VW,

               GS_0, LS_0)(ocl_kernel);

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec =
      std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
  std::cout << std::endl
            << "total runtime for tuning and search space generation = "
            << runtime_in_sec << "sec\n"
            << std::endl;

  std::cout << "saxpy runtime with default parameter values: "
            << ocl_kernel(atf::configuration{}) << std::endl;

  return 0;
}
#endif

#ifdef CLBLAST_GEMM_DIRECT
size_t CeilDiv(const size_t x, const size_t y) { return 1 + ((x - 1) / y); }
size_t Ceil(const size_t x, const size_t y) { return CeilDiv(x, y) * y; }

void fill(std::vector<float> &mat) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<float> dist(1.0, 10.0);

  for (size_t i = 0; i < mat.size(); ++i)
    mat[i] = dist(mt);
}

int main() {
  auto start = std::chrono::system_clock::now();

  std::string gemm_ocl_source =
#include "xgemm_direct.opencl" //"gemm.cl" //
      ;

  // size_t N = 9;
  const int kSizeM = 100; // pow(2,N);
  const int kSizeN = 10;  // pow(2,N);
  const int kSizeK = 1;   // pow(2,N);

  std::vector<float> A(kSizeM * kSizeK);
  fill(A);
  std::vector<float> B(kSizeK * kSizeN);
  fill(B);
  std::vector<float> C(kSizeM * kSizeN);

  std::vector<float> C_gold(kSizeM * kSizeN);
  for (int i = 0; i < kSizeM; ++i)
    for (int j = 0; j < kSizeN; ++j)
      for (int k = 0; k < kSizeK; ++k) {
        C_gold[i * kSizeN + j] += A[i * kSizeK + k] * B[k * kSizeN + j];
        // std::cout << "C_gold[ i * kSizeN + j ] = " << C_gold[ i * kSizeN + j
        // ] << std::endl;
      }

#if 0 // testing
  auto WGD    = atf::tp( "WGD",    {8} );
  auto MDIMCD = atf::tp( "MDIMCD", {8}, atf::divides( WGD ) /*&& atf::divides(kSizeM)*/ );
  auto NDIMCD = atf::tp( "NDIMCD", {8}, atf::divides( WGD ) /*&& atf::divides(kSizeN)*/ );
  auto MDIMAD = atf::tp( "MDIMAD", {8}, atf::divides( WGD ) && [&](auto MDIMAD) { return (MDIMCD*NDIMCD) % MDIMAD == 0; } && [&](auto MDIMAD) { return WGD % ( (MDIMCD*NDIMCD)/MDIMAD ) == 0; } );
  auto NDIMBD = atf::tp( "NDIMBD", {8}, atf::divides( WGD ) && [&](auto NDIMBD) { return (MDIMCD*NDIMCD) % NDIMBD == 0; } && [&](auto NDIMBD) { return WGD % ( (MDIMCD*NDIMCD)/NDIMBD ) == 0; } );
  auto KWID   = atf::tp( "KWID",   {2}, atf::divides( WGD ) );
  auto VWMD   = atf::tp( "VWMD",   {1}, atf::divides(WGD / MDIMCD) && atf::divides(WGD / MDIMAD) );
  auto VWND   = atf::tp( "VWND",   {1}, atf::divides(WGD / NDIMCD) && atf::divides(WGD / NDIMBD) );
  auto PADA   = atf::tp( "PADA",   {0} );
  auto PADB   = atf::tp( "PADB",   {0} );
#endif

#if 0 // OpenTuner
  auto WGD    = atf::tp( "WGD",    {8, 16, 32, 64, 128} );
  auto MDIMCD = atf::tp( "MDIMCD", {8, 16, 32}          );
  auto NDIMCD = atf::tp( "NDIMCD", {8, 16, 32}          );
  auto MDIMAD = atf::tp( "MDIMAD", {8, 16, 32}          );
  auto NDIMBD = atf::tp( "NDIMBD", {8, 16, 32}          );
  auto KWID   = atf::tp( "KWID",   {2, 8, 16}           );
  auto VWMD   = atf::tp( "VWMD",   {1, 2, 4, 8}         );
  auto VWND   = atf::tp( "VWND",   {1, 2, 4, 8}         );
  auto PADA   = atf::tp( "PADA",   {0, 1}               );
  auto PADB   = atf::tp( "PADB",   {0, 1}               );
#endif

#if 0 // CLTune
  auto WGD    = atf::tp( "WGD",    {8, 16, 32, 64, 128} );
  auto MDIMCD = atf::tp( "MDIMCD", {8, 16, 32},     atf::divides( WGD ) /*&& atf::divides(kSizeM)*/ );
  auto NDIMCD = atf::tp( "NDIMCD", {8, 16, 32},     atf::divides( WGD ) /*&& atf::divides(kSizeN)*/ );
  auto MDIMAD = atf::tp( "MDIMAD", {8, 16, 32},     atf::divides( WGD ) && [&](auto MDIMAD) { return (MDIMCD*NDIMCD) % MDIMAD == 0; } && [&](auto MDIMAD) { return WGD % ( (MDIMCD*NDIMCD)/MDIMAD ) == 0; } );
  auto NDIMBD = atf::tp( "NDIMBD", {8, 16, 32},     atf::divides( WGD ) && [&](auto NDIMBD) { return (MDIMCD*NDIMCD) % NDIMBD == 0; } && [&](auto NDIMBD) { return WGD % ( (MDIMCD*NDIMCD)/NDIMBD ) == 0; } );
  auto KWID   = atf::tp( "KWID",   {2, 8, 16},      atf::divides( WGD ) );
  auto VWMD   = atf::tp( "VWMD",   {1, 2, 4, 8},    atf::divides(WGD / MDIMCD) && atf::divides(WGD / MDIMAD) );
  auto VWND   = atf::tp( "VWND",   {1, 2, 4, 8},    atf::divides(WGD / NDIMCD) && atf::divides(WGD / NDIMBD) );
  auto PADA   = atf::tp( "PADA",   {0, 1}  ); 
  auto PADB   = atf::tp( "PADB",   {0, 1}  );
#endif

#if 1 // -> ATF
  const int kSizeMax = std::max(kSizeM, kSizeN);

  auto WGD = atf::tp("WGD", atf::interval<int>(1, kSizeMax));
  auto MDIMCD = atf::tp("MDIMCD", atf::interval<int>(1, kSizeM),
                        atf::divides(WGD) /*&& atf::divides(kSizeM)*/);
  auto NDIMCD = atf::tp("NDIMCD", atf::interval<int>(1, kSizeN),
                        atf::divides(WGD) /*&& atf::divides(kSizeN)*/);
  auto MDIMAD = atf::tp(
      "MDIMAD", atf::interval<int>(1, kSizeM),
      atf::divides(WGD) &&
          [&](auto MDIMAD) { return (MDIMCD * NDIMCD) % MDIMAD == 0; } &&
          [&](auto MDIMAD) { return WGD % ((MDIMCD * NDIMCD) / MDIMAD) == 0; });
  auto NDIMBD = atf::tp(
      "NDIMBD", atf::interval<int>(1, kSizeN),
      atf::divides(WGD) &&
          [&](auto NDIMBD) { return (MDIMCD * NDIMCD) % NDIMBD == 0; } &&
          [&](auto NDIMBD) { return WGD % ((MDIMCD * NDIMCD) / NDIMBD) == 0; });
  auto KWID = atf::tp("KWID", atf::interval<int>(1, kSizeK), atf::divides(WGD));
  auto VWMD = atf::tp("VWMD", {1, 2, 4, 8},
                      atf::divides(WGD / MDIMCD) && atf::divides(WGD / MDIMAD));
  auto VWND = atf::tp(
      "VWND", {1, 2, 4, 8},
      atf::divides(WGD / NDIMCD) && atf::divides(WGD / NDIMBD) && [&](auto) {
        return (((1 + ((kSizeM - 1) / WGD)) * WGD * MDIMCD) / WGD) +
                   (((1 + ((kSizeM - 1) / WGD)) * WGD * MDIMCD) / WGD) <=
               1024;
      });
  auto PADA = atf::tp("PADA", {0, 1});
  auto PADB = atf::tp("PADB", {0, 1});
#endif

  auto PRECISION = atf::tp("PRECISION", {32});

  atf::cf::thread_configurations_t thread_configurations;

#if 0 // XgemmDirectNN
  auto ocl_kernel = atf::cf::ocl( {"Apple",
                                  atf::cf::device_info::GPU,
                                  0},
                                  { gemm_ocl_source , "XgemmDirectNN" },
                                  inputs( atf::scalar<int>(kSizeM),
                                          atf::scalar<int>(kSizeN),
                                          atf::scalar<int>(kSizeK),
                                          atf::scalar<float>(1),                // alpha
                                          atf::scalar<float>(0), //1),                // beta
                                          atf::buffer<float>( A ),
                                          atf::scalar<int>(0),                  // offset M
                                          atf::scalar<int>(kSizeM),                  // a_ld
                                          atf::buffer<float>( B ),
                                          atf::scalar<int>(0),                  // offset N
                                          atf::scalar<int>(kSizeN),                 // b_ld
                                          atf::buffer<float>( C ),
                                          atf::scalar<int>(0),                  // offset K
                                          atf::scalar<int>(kSizeN),                 // c_ld
                                          atf::scalar<int>(1),                  // c_transpose
                                          atf::scalar<int>(0),                  // a_conjugate
                                          atf::scalar<int>(0)                   // b_conjugate
                                        ),
                                  atf::cf::GS( ((1 + ((kSizeM - 1) / WGD))*WGD * MDIMCD) / WGD, ((1 + ((kSizeN - 1) / WGD))*WGD * NDIMCD) / WGD ),
                                  atf::cf::LS(  MDIMCD                ,  NDIMCD                 )
                                ).check_result( A, B, C_gold );
#endif

#if 1 // XgemmDirectTN
  auto ocl_kernel =
      atf::cf::ocl(
          {"Apple", atf::cf::device_info::GPU, 0},
          {gemm_ocl_source, "XgemmDirectTN"},
          inputs(atf::scalar<int>(kSizeM), atf::scalar<int>(kSizeN),
                 atf::scalar<int>(kSizeK),
                 atf::scalar<float>(1), // alpha
                 atf::scalar<float>(0), // 1),                // beta
                 atf::buffer<float>(A),
                 atf::scalar<int>(0),      // offset M
                 atf::scalar<int>(kSizeK), // a_ld
                 atf::buffer<float>(B),
                 atf::scalar<int>(0),      // offset N
                 atf::scalar<int>(kSizeN), // b_ld
                 atf::buffer<float>(C),
                 atf::scalar<int>(0),      // offset K
                 atf::scalar<int>(kSizeN), // c_ld
                 atf::scalar<int>(1),      // c_transpose
                 atf::scalar<int>(0),      // a_conjugate
                 atf::scalar<int>(0)       // b_conjugate
                 ),
          atf::cf::GS(((1 + ((kSizeM - 1) / WGD)) * WGD * MDIMCD) / WGD,
                      ((1 + ((kSizeN - 1) / WGD)) * WGD * NDIMCD) / WGD),
          atf::cf::LS(MDIMCD, NDIMCD))
          .check_result(A, B, C_gold);
#endif

#if 0 // XgemmDirectTT
  auto ocl_kernel = atf::cf::ocl( {"Apple",
                                  atf::cf::device_info::GPU,
                                  0},
                                  { gemm_ocl_source , "XgemmDirectTT" },
                                  inputs( atf::scalar<int>(kSizeM),
                                          atf::scalar<int>(kSizeN),
                                          atf::scalar<int>(kSizeK),
                                          atf::scalar<float>(1),                // alpha
                                          atf::scalar<float>(0),                // beta
                                          atf::buffer<float>( A ), //kSizeM * kSizeK),
                                          atf::scalar<int>(0),                  // offset M
                                          atf::scalar<int>(kSizeK),                  // a_ld
                                          atf::buffer<float>( B ),
                                          atf::scalar<int>(0),                  // offset N
                                          atf::scalar<int>(kSizeK),                 // b_ld
                                          atf::buffer<float>( C ),
                                          atf::scalar<int>(0),                  // offset K
                                          atf::scalar<int>(kSizeN),                 // c_ld
                                          atf::scalar<int>(1),                  // c_transpose
                                          atf::scalar<int>(0),                  // a_conjugate
                                          atf::scalar<int>(0)                   // b_conjugate
                                        ),
                                  atf::cf::GS( ((1 + ((kSizeM - 1) / WGD))*WGD * MDIMCD) / WGD, ( (1 + ((kSizeN - 1) / WGD))*WGD * NDIMCD) / WGD ),
                                  atf::cf::LS(  MDIMCD                ,  NDIMCD                 )
                                ).check_result( A, B, C_gold );
#endif

  auto best_config =
      atf::exhaustive //<NO_CONSTRAINTS>()
      // atf::open_tuner//<NO_CONSTRAINTS>
      // atf::open_tuner_flat<NO_CONSTRAINTS>
      // atf::annealing_tree<NO_CONSTRAINTS>
      // atf::annealing<NO_CONSTRAINTS>
      //( atf::cond::evaluations(1000) )
      (atf::cond::speedup(1, 200) || atf::cond::evaluations(1000))
  //( atf::cond::speedup(1, 200) )

#if 0 // with G(...)
  ( G(WGD,
      MDIMCD,
      NDIMCD,
      MDIMAD,
      NDIMBD,
      KWID,
      VWMD,
      VWND
     ),
    G( PADA ),
    G( PADB ),
    G( PRECISION )
  )
  ( ocl_kernel );
#else // without G(...)
          (WGD, MDIMCD, NDIMCD, MDIMAD, NDIMBD, KWID, VWMD, VWND, PADA, PADB,
           PRECISION)(ocl_kernel);
#endif

          std::cout
      << "\nbest found configuration: ";
  for (auto &tp : best_config)
    std::cout << tp.first << " = " << tp.second << std::endl;

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec =
      std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
  std::cout << std::endl
            << "total runtime for tuning and search space generation = "
            << runtime_in_sec << "sec\n"
            << std::endl;

  return 0;
}
#endif

#ifdef CLTUNE_MATMULT
void mat_mult(std::vector<float> &A, std::vector<float> &B,
              std::vector<float> &C, size_t N) {
  for (size_t i = 0; i < N; ++i)
    for (size_t j = 0; j < N; ++j)
      for (size_t k = 0; k < N; ++k)
        C[i * N + j] = A[i * N + k] * B[k * N + j];
}

void fill(std::vector<float> &mat) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<float> dist(1.0, 10.0);

  for (size_t i = 0; i < mat.size(); ++i)
    mat[i] = dist(mt);
}

int main() {
  auto start = std::chrono::system_clock::now();

  std::string gemm_ocl_source =
#include "gemm.cl"
      ;

  const int DIM_M = 7; // 100 -> 128
  const int DIM_N = 5; // 10  -> 16
  const int DIM_K = 9; // 500 -> 512

  const int kSizeM = pow(2, DIM_M);
  const int kSizeN = pow(2, DIM_N);
  const int kSizeK = pow(2, DIM_K);

  std::vector<float> C(kSizeM * kSizeN);
  std::vector<float> A(kSizeM * kSizeK);
  std::vector<float> B(kSizeK * kSizeN);

  fill(A);
  fill(B);

#if 0 // testing
  auto MWG   = atf::tp( "MWG"  , {16}  );
  auto NWG   = atf::tp( "NWG"  , {16}  );
  auto KWG   = atf::tp( "KWG"  , {16}                                  );
  auto MDIMC = atf::tp( "MDIMC", {8},       atf::divides(MWG)  );
  auto NDIMC = atf::tp( "NDIMC", {8},       atf::divides(NWG)  );
  auto MDIMA = atf::tp( "MDIMA", {8},       atf::divides(MWG) && [&](auto MDIMA) { return (MDIMC*NDIMC) % MDIMA == 0; } && [&](auto MDIMA) { return KWG % ( (MDIMC*NDIMC) / MDIMA ) == 0; }  );
  auto NDIMB = atf::tp( "NDIMB", {8},       atf::divides(NWG) && [&](auto NDIMB) { return (MDIMC*NDIMC) % NDIMB == 0; } && [&](auto NDIMB) { return KWG % ( (MDIMC*NDIMC) / NDIMB ) == 0; }  );
  auto KWI   = atf::tp( "KWI"  , {2},            atf::divides( KWG ) );
  auto VWM   = atf::tp( "VWM"  , {1},         atf::divides(MWG / MDIMC) && atf::divides(MWG / MDIMA) );
  auto VWN   = atf::tp( "VWN"  , {1},         atf::divides(NWG / NDIMC) && atf::divides(NWG / NDIMB) && [&](auto){ return ( (kSizeM * MDIMC) / MWG ) + ( (kSizeN * NDIMC) / NWG ) <= 1024; } );
  auto STRM  = atf::tp( "STRM" , {0}             );
  auto STRN  = atf::tp( "STRN" , {0}             );
  auto SA    = atf::tp( "SA"   , {0}             );
  auto SB    = atf::tp( "SB"   , {0}             );
#endif

#if 0 // CLTune
  auto MWG   = atf::tp( "MWG"  , {16, 32, 64, 128}  );
  auto NWG   = atf::tp( "NWG"  , {16, 32, 64, 128}  );
  auto KWG   = atf::tp( "KWG"  , {16, 32}                                  );
  auto MDIMC = atf::tp( "MDIMC", {8, 16, 32},       atf::divides(MWG)  );
  auto NDIMC = atf::tp( "NDIMC", {8, 16, 32},       atf::divides(NWG)  );
  auto MDIMA = atf::tp( "MDIMA", {8, 16, 32},       atf::divides(MWG) && [&](auto MDIMA) { return (MDIMC*NDIMC) % MDIMA == 0; } && [&](auto MDIMA) { return KWG % ( (MDIMC*NDIMC) / MDIMA ) == 0; }  );
  auto NDIMB = atf::tp( "NDIMB", {8, 16, 32},       atf::divides(NWG) && [&](auto NDIMB) { return (MDIMC*NDIMC) % NDIMB == 0; } && [&](auto NDIMB) { return KWG % ( (MDIMC*NDIMC) / NDIMB ) == 0; }  );
  auto KWI   = atf::tp( "KWI"  , {2, 8},            atf::divides( KWG ) );
  auto VWM   = atf::tp( "VWM"  , {1,2,4,8},         atf::divides(MWG / MDIMC) && atf::divides(MWG / MDIMA) );
  auto VWN   = atf::tp( "VWN"  , {1,2,4,8},         atf::divides(NWG / NDIMC) && atf::divides(NWG / NDIMB) && [&](auto){ return ( (kSizeM * MDIMC) / MWG ) + ( (kSizeN * NDIMC) / NWG ) <= 1024; } );
  auto STRM  = atf::tp( "STRM" , {0, 1}             );
  auto STRN  = atf::tp( "STRN" , {0, 1}             );
  auto SA    = atf::tp( "SA"   , {0, 1}             );
  auto SB    = atf::tp( "SB"   , {0, 1}             );
#endif

#if 0 // OT
  auto MWG   = atf::tp( "MWG"  , atf::interval<int>(0,DIM_M, atf::pow_2) );
  auto NWG   = atf::tp( "NWG"  , atf::interval<int>(0,DIM_N, atf::pow_2) );
  auto KWG   = atf::tp( "KWG"  , atf::interval<int>(0,DIM_K, atf::pow_2) );
  auto MDIMC = atf::tp( "MDIMC", atf::interval<int>(0,DIM_M, atf::pow_2) );
  auto NDIMC = atf::tp( "NDIMC", atf::interval<int>(0,DIM_N, atf::pow_2) );
  auto MDIMA = atf::tp( "MDIMA", atf::interval<int>(0,DIM_M, atf::pow_2) );
  auto NDIMB = atf::tp( "NDIMB", atf::interval<int>(0,DIM_N, atf::pow_2) );
  auto KWI   = atf::tp( "KWI"  , atf::interval<int>(0,DIM_K, atf::pow_2) );
  auto VWM   = atf::tp( "VWM"  , {1,2,4,8}                               );
  auto VWN   = atf::tp( "VWN"  , {1,2,4,8}                               );
  auto STRM  = atf::tp( "STRM" , {0, 1}                                  );
  auto STRN  = atf::tp( "STRN" , {0, 1}                                  );
  auto SA    = atf::tp( "SA"   , {0, 1}                                  );
  auto SB    = atf::tp( "SB"   , {0, 1}                                  );
#endif

#if 1 // -> ATF
  auto MWG = atf::tp("MWG", atf::interval<int>(0, DIM_M, atf::pow_2));
  auto NWG = atf::tp("NWG", atf::interval<int>(0, DIM_N, atf::pow_2));
  auto KWG = atf::tp("KWG", atf::interval<int>(0, DIM_K, atf::pow_2));
  auto MDIMC = atf::tp("MDIMC", atf::interval<int>(0, DIM_M, atf::pow_2),
                       atf::divides(MWG));
  auto NDIMC = atf::tp("NDIMC", atf::interval<int>(0, DIM_N, atf::pow_2),
                       atf::divides(NWG));
  auto MDIMA = atf::tp(
      "MDIMA", atf::interval<int>(0, DIM_M, atf::pow_2),
      atf::divides(MWG) &&
          [&](auto MDIMA) { return (MDIMC * NDIMC) % MDIMA == 0; } &&
          [&](auto MDIMA) { return KWG % ((MDIMC * NDIMC) / MDIMA) == 0; });
  auto NDIMB = atf::tp(
      "NDIMB", atf::interval<int>(0, DIM_N, atf::pow_2),
      atf::divides(NWG) &&
          [&](auto NDIMB) { return (MDIMC * NDIMC) % NDIMB == 0; } &&
          [&](auto NDIMB) { return KWG % ((MDIMC * NDIMC) / NDIMB) == 0; });
  auto KWI = atf::tp("KWI", atf::interval<int>(0, DIM_K, atf::pow_2),
                     atf::divides(KWG));
  auto VWM = atf::tp("VWM", {1, 2, 4, 8},
                     atf::divides(MWG / MDIMC) && atf::divides(MWG / MDIMA));
  auto VWN = atf::tp(
      "VWN", {1, 2, 4, 8},
      atf::divides(NWG / NDIMC) && atf::divides(NWG / NDIMB) && [&](auto) {
        return ((kSizeM * MDIMC) / MWG) + ((kSizeN * NDIMC) / NWG) <= 1024;
      });
  auto STRM = atf::tp("STRM", {0, 1});
  auto STRN = atf::tp("STRN", {0, 1});
  auto SA = atf::tp("SA", {0, 1});
  auto SB = atf::tp("SB", {0, 1});
#endif

  auto PRECISION = atf::tp("PRECISION", {32});

  atf::cf::thread_configurations_t thread_configurations;

  auto ocl_kernel =
      atf::cf::ocl({"NVIDIA", atf::cf::device_info::GPU, 0}, gemm_ocl_source,
                   inputs(atf::scalar<int>(kSizeM), atf::scalar<int>(kSizeN),
                          atf::scalar<int>(kSizeK),
                          atf::buffer<float>(kSizeM * kSizeK),
                          atf::buffer<float>(kSizeK * kSizeN),
                          atf::buffer<float>(kSizeM * kSizeN)),
                   atf::cf::GS((kSizeM * MDIMC) / MWG, (kSizeN * NDIMC) / NWG),
                   atf::cf::LS(MDIMC, NDIMC))
          .save_thread_configuration(thread_configurations);

  auto best_config =
      // atf::exhaustive()
      atf::open_tuner
      //( atf::cond::valid_evaluations(117) )
      (atf::cond::speedup(1, 100) || atf::cond::evaluations(1000))(
          G(MWG, NWG, KWG, MDIMC, NDIMC, MDIMA, NDIMB, KWI, VWM, VWN), G(STRM),
          G(STRN), G(SA), G(SB), G(PRECISION))(ocl_kernel);

  for (auto &tp : best_config)
    std::cout << tp.first << " = " << tp.second << std::endl;

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec =
      std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
  std::cout << std::endl
            << "total runtime for tuning and search space generation = "
            << runtime_in_sec << "sec\n"
            << std::endl;

  auto dummy_config = atf::configuration{};
  std::cout << "saxpy runtime with default parameter values: "
            << ocl_kernel(dummy_config) << std::endl;

  //  auto thread_configuration = thread_configurations[ best_config ];
  //  for( size_t i = 0 ; i < 2 ; ++i )
  //  {
  //    for( size_t j = 0 ; j < 3 ; ++j )
  //      std::cout << thread_configuration[ i ][ j ] << " ";
  //    std::cout << std::endl;
  //  }

  return 0;
}
#endif

#ifdef CLTUNE_CONV
// Helper function to perform an integer division + ceiling (round-up)
size_t CeilDiv(size_t a, size_t b) { return (a + b - 1) / b; }

// Helper function to determine whether or not 'a' is a multiple of 'b'
bool IsMultiple(size_t a, size_t b) {
  return ((a / b) * b == a) ? true : false;
};

#define HFS (3)            // Half filter size
#define FS (HFS + HFS + 1) // Filter size

int main() {
  auto start = std::chrono::system_clock::now();

  std::string conv_ocl_source =
#include "conv.cl"
      ;

  const auto kExtraSize = size_t{FS * 8};
  //  const int N = 1024;

  const int kSizeX = 8192;
  const int kSizeY = 4096;

  auto mat_a =
      std::vector<float>((kSizeX + kExtraSize) * (kSizeY + kExtraSize), 1);
  auto mat_b = std::vector<float>(kSizeX * kSizeY, 0);
  auto coeff = std::vector<float>(FS * FS);

  // Creates the filter coefficients (gaussian blur)
  auto sigma = 1.0f;
  auto mean = FS / 2.0f;
  auto sum = 0.0f;
  for (auto x = size_t{0}; x < FS; ++x) {
    for (auto y = size_t{0}; y < FS; ++y) {
      auto exponent = -0.5f * (pow((x - mean) / sigma, 2.0f) +
                               pow((y - mean) / sigma, 2.0f));
      coeff[y * FS + x] = static_cast<float>(
          exp(exponent) / (2.0f * 3.14159265f * sigma * sigma));
      sum += coeff[y * FS + x];
    }
  }
  for (auto &item : coeff) {
    item = item / sum;
  }

#if 1
  auto TBX = atf::tp("TBX", {8, 16, 32, 64});
  auto TBY = atf::tp("TBY", {8, 16, 32, 64});
  auto LOCAL = atf::tp("LOCAL", {0, 1, 2});
  auto WPTX = atf::tp("WPTX", {1, 2, 4, 8});
  auto WPTY = atf::tp("WPTY", {1, 2, 4, 8});

  auto VECTOR_constraint = [&](auto VECTOR) {
    if (LOCAL == 2)
      return IsMultiple(WPTX, VECTOR) && IsMultiple(2 * HFS, VECTOR);
    else
      return IsMultiple(WPTX, VECTOR);
  };
  auto VECTOR = atf::tp("VECTOR", {1, 2, 4}, VECTOR_constraint);
  auto UNROLL_FACTOR = atf::tp("UNROLL_FACTOR", {1, FS});

  auto PADDING_constraint = [&](auto PADDING) {
    return (PADDING == 0 || LOCAL != 0);
  };

  // auto LocalMemorySize = [] (std::vector<size_t> v) {
  //    if (LOCAL != 0) { return ((TBY*WPTY + 2*HFS) * (TBX*WPTX + 2*HFS +
  //    self))*sizeof(float); } else           { return size_t{0}; }
  //  };
  //  tuner.SetLocalMemoryUsage(id, LocalMemorySize, {"LOCAL", "TBX", "WPTX",
  //  "TBY", "WPTY", "PADDING"});

  auto PADDING = atf::tp("PADDING", {0, 1}, PADDING_constraint);

  auto integers = std::initializer_list<size_t>{
      8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
      22, 23, 24, 25, 26, 32, 33, 34, 35, 36, 37, 38, 39, 40,
      41, 42, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74};

  auto TBX_XL_constraint = [&](auto TBX_XL) -> bool {
    if (LOCAL == 2)
      return (TBX_XL == TBX + CeilDiv(2 * HFS, WPTX));
    else
      return (TBX_XL == TBX);
  };
  auto TBX_XL = atf::tp("TBX_XL", integers, TBX_XL_constraint);

  auto TBY_XL_constraint = [&](auto TBY_XL) -> bool {
    if (LOCAL == 2)
      return (TBY_XL == TBY + CeilDiv(2 * HFS, WPTY));
    else
      return (TBY_XL == TBY);
  };
  auto TBY_XL = atf::tp("TBY_XL", integers, TBY_XL_constraint);

  auto GS_0 = atf::tp("GS_0", atf::interval<size_t>(1, 1, [&](auto) {
                        return ((kSizeX * TBX_XL) / TBX) / WPTX;
                      }));
  auto GS_1 = atf::tp("GS_1", atf::interval<size_t>(1, 1, [&](auto) {
                        return ((kSizeY * TBY_XL) / TBY) / WPTY;
                      }));

  auto LS_0 = atf::tp(
      "LS_0", atf::interval<size_t>(1, 1, [&](auto) { return TBX_XL; }));
  auto LS_1 =
      atf::tp("LS_1", atf::interval<size_t>(1, 1, [&](auto) { return TBY_XL; }),
              [&](auto i) { return LS_0 * i <= 1024; });

#endif

#if 0
  auto TBX   = atf::tp( "TBX"  , atf::interval(3, 12, atf::pow_2) );
  auto TBY   = atf::tp( "TBY"  , atf::interval(3, 13, atf::pow_2) );
  auto LOCAL = atf::tp( "LOCAL", {0, 1, 2}       );
  auto WPTX  = atf::tp( "WPTX" , {1, 2, 4, 8}    );
  auto WPTY  = atf::tp( "WPTY" , {1, 2, 4, 8}    );

  auto VECTOR_constraint = [&]( auto VECTOR )
                           {
                             if( LOCAL == 2 )
                               return IsMultiple( WPTX, VECTOR ) && IsMultiple( 2*HFS , VECTOR );
                             else
                               return IsMultiple( WPTX, VECTOR );
                            };
  auto VECTOR        = atf::tp( "VECTOR"       , {1, 2, 4}, VECTOR_constraint );
  auto UNROLL_FACTOR = atf::tp( "UNROLL_FACTOR", {1, FS}                      );

  auto PADDING_constraint = [&]( auto PADDING )
                            {
                              return ( PADDING == 0 || LOCAL != 0 );
                            };
  
  auto PADDING = atf::tp( "PADDING", {0, 1}, PADDING_constraint );
  
  auto integers = std::initializer_list<size_t>{
    8,9,10,11,12,13,14,15,                16, 17, 18,
    16,17,18,19,20,21,22,23,24,25,26,
    32,33,34,35,36,37,38,39,40,41,42,
    64,65,66,67,68,69,70,71,72,73,74,

    128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,
    256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266,
    512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522,
    1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034,
    2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058
  };
  
  auto TBX_XL_constraint = [&]( auto TBX_XL ) -> bool
                           {
                             if( LOCAL == 2 )
                               return ( TBX_XL == TBX + CeilDiv( 2*HFS, WPTX ) && ( (kSizeX * TBX_XL) >= TBX) && ( ((kSizeX * TBX_XL) / TBX ) % WPTX == 0  ) );
                             else
                               return ( (TBX_XL == TBX) && ( (kSizeX * TBX_XL) >= TBX) && ( ((kSizeX * TBX_XL) / TBX ) % WPTX == 0  ) );
                           };
  auto TBX_XL = atf::tp( "TBX_XL", integers, TBX_XL_constraint );
  
  auto TBY_XL_constraint = [&]( auto TBY_XL ) -> bool
                           {
                             if( LOCAL == 2 )
                               return ( TBY_XL == TBY + CeilDiv( 2*HFS, WPTY ) && ( (kSizeY * TBY_XL) >= TBY) && ( ((kSizeY * TBY_XL) / TBY ) % WPTY == 0  ) );
                             else
                               return ( (TBY_XL == TBY) && ( (kSizeY * TBY_XL) >= TBY) && ( ((kSizeY * TBY_XL) / TBY ) % WPTY == 0  ) );
                           };
  auto TBY_XL = atf::tp( "TBY_XL", integers, TBY_XL_constraint );


  auto GS_0 = atf::tp( "GS_0", atf::interval<size_t>(1,1, [&]( auto ) { return ( (kSizeX * TBX_XL) / TBX ) / WPTX; } ) );
  auto GS_1 = atf::tp( "GS_1", atf::interval<size_t>(1,1, [&]( auto ) { return ( (kSizeY * TBY_XL) / TBY ) / WPTY; } ) );
  
  auto LS_0 = atf::tp( "LS_0", atf::interval<size_t>(1,1, [&]( auto ) { return TBX_XL; } ) );
  auto LS_1 = atf::tp( "LS_1", atf::interval<size_t>(1,1, [&]( auto ) { return TBY_XL; } ), [&](auto i){ return LS_0*i<=1024;} );

#endif

  auto ocl_kernel =
      atf::cf::ocl("Apple", CL_DEVICE_TYPE_GPU, 0, conv_ocl_source,
                   inputs(atf::scalar<int>(kSizeX), atf::scalar<int>(kSizeY),
                          atf::buffer<float>(mat_a), atf::buffer<float>(coeff),
                          atf::buffer<float>(mat_b)));

  atf::open_tuner(atf::cond::valid_test_count(10))
      .set_TPs(TBX, TBY, LOCAL, WPTX, WPTY, VECTOR,
               UNROLL_FACTOR, // in own group G(...)
               PADDING, TBX_XL, TBY_XL, GS_0, GS_1, LS_0, LS_1)(ocl_kernel);

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec =
      std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
  std::cout << std::endl
            << "total runtime for tuning and search space generation = "
            << runtime_in_sec << "sec\n"
            << std::endl;

  return 0;
}
#endif

#ifdef SEARCH_SPACE_GENERATION_TEST_USING_GEMM
size_t CeilDiv(const size_t x, const size_t y) { return 1 + ((x - 1) / y); }
size_t Ceil(const size_t x, const size_t y) { return CeilDiv(x, y) * y; }

void fill(std::vector<float> &mat) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<float> dist(1.0, 10.0);

  for (size_t i = 0; i < mat.size(); ++i)
    mat[i] = dist(mt);
}

int main(int argc, char *argv[]) {
  auto start = std::chrono::system_clock::now();

  size_t N = 8; // static_cast<size_t>(std::stoi(std::string{argv[1]}));
  const int kSizeM = pow(2, N);
  const int kSizeN = pow(2, N);
  const int kSizeK = pow(2, N);

  std::vector<float> A(kSizeM * kSizeK);
  fill(A);
  std::vector<float> B(kSizeK * kSizeN);
  fill(B);
  std::vector<float> C(kSizeM * kSizeN);
  fill(C);

  //#if 0 // CLTune
  //  auto WGD    = atf::tp( "WGD",    {8, 16, 32, 64, 128} );
  //  auto MDIMCD = atf::tp( "MDIMCD", {8, 16, 32},     atf::divides( WGD ) /*&&
  //  atf::divides(kSizeM)*/ ); auto NDIMCD = atf::tp( "NDIMCD", {8, 16, 32},
  //  atf::divides( WGD ) /*&& atf::divides(kSizeN)*/ ); auto MDIMAD = atf::tp(
  //  "MDIMAD", {8, 16, 32},     atf::divides( WGD ) && [&](auto MDIMAD) {
  //  return (MDIMCD*NDIMCD) % MDIMAD == 0; } && [&](auto MDIMAD) { return WGD %
  //  ( (MDIMCD*NDIMCD)/MDIMAD ) == 0; } ); auto NDIMBD = atf::tp( "NDIMBD", {8,
  //  16, 32},     atf::divides( WGD ) && [&](auto NDIMBD) { return
  //  (MDIMCD*NDIMCD) % NDIMBD == 0; } && [&](auto NDIMBD) { return WGD % (
  //  (MDIMCD*NDIMCD)/NDIMBD ) == 0; } ); auto KWID   = atf::tp( "KWID",   {2,
  //  8, 16},      atf::divides( WGD ) ); auto VWMD   = atf::tp( "VWMD",   {1,
  //  2, 4, 8},    atf::divides(WGD / MDIMCD) && atf::divides(WGD / MDIMAD) );
  //  auto VWND   = atf::tp( "VWND",   {1, 2, 4, 8},    atf::divides(WGD /
  //  NDIMCD) && atf::divides(WGD / NDIMBD) ); auto PADA   = atf::tp( "PADA",
  //  {0, 1}  ); auto PADB   = atf::tp( "PADB",   {0, 1}  );
  //#endif

#if 1 // -> ATF
  const int kSizeMax = std::max(kSizeM, kSizeN);

  auto WGD = atf::tp("WGD", atf::interval<int>(1, kSizeMax));
  auto MDIMCD = atf::tp("MDIMCD", atf::interval<int>(1, kSizeM),
                        atf::divides(WGD) /*&& atf::divides(kSizeM)*/);
  auto NDIMCD = atf::tp("NDIMCD", atf::interval<int>(1, kSizeN),
                        atf::divides(WGD) /*&& atf::divides(kSizeN)*/);
  auto MDIMAD = atf::tp(
      "MDIMAD", atf::interval<int>(1, kSizeM),
      atf::divides(WGD) &&
          [&](auto MDIMAD) { return (MDIMCD * NDIMCD) % MDIMAD == 0; } &&
          [&](auto MDIMAD) { return WGD % ((MDIMCD * NDIMCD) / MDIMAD) == 0; });
  auto NDIMBD = atf::tp(
      "NDIMBD", atf::interval<int>(1, kSizeN),
      atf::divides(WGD) &&
          [&](auto NDIMBD) { return (MDIMCD * NDIMCD) % NDIMBD == 0; } &&
          [&](auto NDIMBD) { return WGD % ((MDIMCD * NDIMCD) / NDIMBD) == 0; });
  auto KWID = atf::tp("KWID", atf::interval<int>(1, kSizeK), atf::divides(WGD));
  auto VWMD = atf::tp("VWMD", {1, 2, 4, 8},
                      atf::divides(WGD / MDIMCD) && atf::divides(WGD / MDIMAD));
  auto VWND = atf::tp(
      "VWND", {1, 2, 4, 8},
      atf::divides(WGD / NDIMCD) && atf::divides(WGD / NDIMBD) && [&](auto) {
        return (((1 + ((kSizeM - 1) / WGD)) * WGD * MDIMCD) / WGD) +
                   (((1 + ((kSizeM - 1) / WGD)) * WGD * MDIMCD) / WGD) <=
               1024;
      });
  auto PADA = atf::tp("PADA", {0, 1});
  auto PADB = atf::tp("PADB", {0, 1});
#endif

  auto PRECISION = atf::tp("PRECISION", {32});

  auto dummy_kernel = [](auto) { return 0; };
  auto best_config = atf::exhaustive(atf::cond::evaluations(0))(
      G(WGD, MDIMCD, NDIMCD, MDIMAD, NDIMBD, KWID, VWMD, VWND), G(PADA),
      G(PADB), G(PRECISION))(dummy_kernel);

  auto end = std::chrono::system_clock::now();
  auto runtime_in_ms =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
          .count();
  std::cout << std::endl
            << "total runtime for search space generation = " << runtime_in_ms
            << "ms\n"
            << std::endl;

  return 0;
}

#endif

#ifdef MD_HOM_GEMV
int main() {
  auto start = std::chrono::system_clock::now();

  // input size
  const int M = pow(2, 14);
  const int N = pow(2, 14);

  // global/local-sizes
  auto NUM_WG_0 = atf::tp("NUM_WG_0", atf::interval<size_t>(1, N));
  auto NUM_WI_0 = atf::tp("NUM_WI_0", atf::interval<size_t>(1, N),
                          atf::divides(N / NUM_WG_0));

  // global/local-sizes
  auto NUM_WG_1 = atf::tp("NUM_WG_1", atf::interval<size_t>(1, M));
  auto NUM_WI_1 = atf::tp("NUM_WI_1", atf::interval<size_t>(1, M),
                          atf::divides(M / NUM_WG_1));

  // cost function (TODO: CLBlast wrapper)
  auto dummy = [](auto) { return 0; };

  // tuning
  atf::exhaustive(atf::cond::valid_evaluations(0))(
      G(NUM_WG_0, NUM_WI_0), G(NUM_WG_1, NUM_WI_1))(dummy);

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec =
      std::chrono::duration_cast<std::chrono::seconds>(end - start).count();
  std::cout << std::endl
            << "total runtime for tuning and search space generation = "
            << runtime_in_sec << "sec\n"
            << std::endl;

  return 0;
}
#endif
