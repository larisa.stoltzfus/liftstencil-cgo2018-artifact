This artifact consist of many software components each with their own licences.

This file list the software components and their LICENSE files.

 * Lift in the folder ./tools/lift with the licence file at ./tools/lift/LICENSE

 * ATF in the folder ./tools/atf with the licence files at ./tools/atf/atf/LICENSE.md and ./tools/atf/atfc/LICENSE.md
 
 * PPCG in the folder ./tools/ppcg with the licence file at ./tools/ppcg/LICENSE
 
 * SHOC in the folders ./include/shoc with the licence file at ./include/shoc/LICENSE.txt
 
 * Rodinia benchmarks in these folders:
    - ./benchmarks/figure7/workflow1/reference/srad
    - ./input_data/srad
    - ./benchmarks/figure7/workflow1/reference/hotspot
    - ./input_data/hotspot
    - ./benchmarks/figure7/workflow1/reference/hostspot3D
    - ./input_data/hotspot3D
   with coresponding license files in the folders:
    - ./benchmarks/figure7/workflow1/reference/srad
    - ./benchmarks/figure7/workflow1/reference/hotspot
    - ./benchmarks/figure7/workflow1/reference/hostspot3D

 * Acoustic benchmark in the folder ./benchmarks/figure7/workflow1/reference/acoustic
   with a licence file at ./benchmarks/figure7/workflow1/reference/acoustic/LICENSE
 
 * SDSLC Benchmarks in these folders with a coresponding license file in each folder:
    - ./benchmarks/figure8/workflow1/gaussian/big
    - ./benchmarks/figure8/workflow1/gaussian/small
    - ./benchmarks/figure8/workflow1/grad2d/big
    - ./benchmarks/figure8/workflow1/grad2d/small
    - ./benchmarks/figure8/workflow1/heat3d/big
    - ./benchmarks/figure8/workflow1/heat3d/small
    - ./benchmarks/figure8/workflow1/j2d5pt/big
    - ./benchmarks/figure8/workflow1/j2d5pt/small
    - ./benchmarks/figure8/workflow1/j2d9pt/big
    - ./benchmarks/figure8/workflow1/j2d9pt/small
    - ./benchmarks/figure8/workflow1/j3d13pt/big
    - ./benchmarks/figure8/workflow1/j3d13pt/small
    - ./benchmarks/figure8/workflow1/j3d7pt/big
    - ./benchmarks/figure8/workflow1/j3d7pt/small
    - ./benchmarks/figure8/workflow1/poisson3d/big
    - ./benchmarks/figure8/workflow1/poisson3d/small

The following files are licensed under the MIT license (shown below):
 * ./README.md
 * ./environment.env
 * all files in the folder ./benchmarks/figure7/workflow1/lift
 * all files in the folder ./benchmarks/figure7/workflow2/
 * all files in the folder ./benchmarks/figure8/workflow2/
 * all files in the folder ./scripts
 
For usage of any other files please contact the authors of this artifact:
 - Bastian Hagedorn, University of Muenster (b.hagedorn@wwu.de)
 - Larisa Stoltzfus, University of Edinburgh (larisa.stoltzfus@ed.ac.uk)
 - Michel Steuwer, University of Glasgow (michel.steuwer@glasgow.ac.uk)
 - Sergei Gorlatch, University of Muenster (gorlatch@wwu.de)
 - Christophe Dubach, University of Edinburgh (christophe.dubach@ed.ac.uk)
 
================================================================================
MIT License

Copyright (c) 2018 Bastian Hagedorn, Larisa Stoltzfus, Michel Steuwer,
                   Sergei Gorlatch, Christophe Dubach

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
